#ifndef ASTAR_H
#define ASTAR_H

/*********************************************************************************
*     File Name           :     AStar.h
*     Created By          :     robin
*     Creation Date       :     [2016-11-16 22:08]
*     Last Modified       :     [2017-01-12 02:32]
*     Description         :      
**********************************************************************************/

#include "Mesh.h"
#include <math.h>

enum OCList {IntoOpenedList=1,IntoClosedList=2,NoList=3};
typedef enum OCList OCList;



typedef struct ANode ANode;

struct ANode
{
	Node *A_Node;

	ANode *NextNode;
	ANode *Father;
	double f; //Cette grandeur s'appelle vraiment f  (f(A)=g(A)+h(A))
};


typedef struct AList AList;

struct AList    // Simple liste chainée 1D pour éviter d'avoir à utiliser des tableau
		// un pointeur de pointeur à realoc tout le temps
{
	ANode *Opened0;
	ANode *OpenedN;
	ANode *Closed0;
	ANode *ClosedN;

	ANode *Path0; //Point de départ
	ANode *PathN; //Point d'arrivé

	//Pointeurs de fonctions
	double (*g)(Node*,Node*); //Fonction g, renvois la distance entre le noeud considéré et le 
		   //noeud de départ
		   //nb: Dans le cas d'un maillage 2 axes (Haut, Bas, Gauche, Droite)
		   //g est la distance de Manhattan entre le départ et le point 
		   //considéré
	double (*h)(Node*,Node*); //Fonction heuristique, EVALUE la distance entre le départ et
		   //l'arrivée

};




void InitAList(AList *List,Node *FirstNode,double (*g)(Node*,Node*),double (*h)(Node*,Node*));

void AddNodeOpenedList(AList *List,Node *A);
void AddNodeClosedList(AList *List,Node *A);

void AddNodePath(AList *List,Node *A);


void DelNodeOpenedList(AList *List,Node *A); //On cherche et supprime le noeud A
void DelNodeClosedList(AList *List,Node *A);

void DelAList(AList *List);

char IsInOpenedList(AList *List,Node *A); //Booléns
char IsInClosedList(AList *List,Node *A);
char IsInPah(AList *List,Node *A);

double EulerDistance(Node *A,Node *B); // Ces fonctions renvoient toujours une grandeur
				       // en m et non en unité de maillage
double ManhattanDistance(Node *A,Node *B);

double NullDistance(Node* A,Node* B);


void UpDateOpenedList(AList *List,ANode *Ref); //Cherche et ajoute les noeuds valide adjacents à Ref
ANode *SelectNode(AList *List); //Cherche le noeud avec f minimal dans l'opened list
void ApplyF(AList *List,Node *Ref,Node *End);
void PathFinding(FlagMap *Map,AList *List,Node *Start,Node *End,double (*g)(Node*,Node*),double (*h)(Node*,Node*));

// DEBUG
void DebugOpenedList(AList *List);
void DebugClosedList(AList *List);
void DebugPath(AList *List);

/*
double gEulerDistance(Node *A);
double gManhattanDistance(Node*A);

double hManhattanDistance(Node*A);
*/







#endif /* ASTAR_H */
