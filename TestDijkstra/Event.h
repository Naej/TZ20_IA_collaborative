#ifndef EVENT_H
#define EVENT_H

/*********************************************************************************
*     File Name           :     Event.h
*     Created By          :     robin
*     Creation Date       :     [2016-12-16 15:12]
*     Last Modified       :     [2016-12-21 14:36]
*     Description         :     Structure pour l'IA 
**********************************************************************************/

#include "Condition.h"


typedef struct Event Event;


struct Event
{
	
	Event *CorrespondenceList;
	Condition *ConditionList;
	
};



#endif /* EVENT_H */
