/*********************************************************************************
*     File Name           :     Presentation.c
*     Created By          :     robin
*     Creation Date       :     [2017-01-11 20:26]
*     Last Modified       :     [2017-01-15 02:11]
*     Description         :      
**********************************************************************************/
#include <math.h>
#include "stdio.h"
#include "stdlib.h"
#include "Mesh.h"
#include "Presentation.h"
#include "Screen.h"
#include "AStar.h"
#include "Draw.h"

void SetLineObstacle(FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2 )
{

	
	//Expression des paramètres x,y du segment
	//de vecteur directeur normalisé (a,b)
	Node *Cursor=NULL;
	
	

	double i=0.f;

	double x=0.f;
	double y=0.f;
	double Length=sqrt((Gridy1-Gridy2)*(Gridy1-Gridy2)+(Gridx1-Gridx2)*(Gridx1-Gridx2));

	double a=(Gridx2-Gridx1)/(Length);
	double b=(Gridy2-Gridy1)/(Length);

	

	for(i=0;i<=Length;i++)
	{
		x=i*a+Gridx1;
		y=i*b+Gridy1;
		Cursor=GetNode(Map,(long int)x,(long int)y);
		if(Cursor)
		{
			Cursor->Kind=Obstacle;
		}

	}	

}


void AddNodeLine(FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2)
{
		//Expression des paramètres x,y du segment
	//de vecteur directeur normalisé (a,b)
	Node *Cursor=NULL;
	
	

	double i=0.f;

	double x=0.f;
	double y=0.f;
	double Length=sqrt((Gridy1-Gridy2)*(Gridy1-Gridy2)+(Gridx1-Gridx2)*(Gridx1-Gridx2));

	double a=(Gridx2-Gridx1)/(Length);
	double b=(Gridy2-Gridy1)/(Length);

	

	for(i=0;i<=Length;i++)
	{
		x=i*a+Gridx1;
		y=i*b+Gridy1;
		Cursor=GetNode(Map,(long int)x,(long int)y);
		if(Cursor==NULL)
		{
			//Cursor=(Node*)malloc()
		}

	}	


}

void MapU(FlagMap *Map)
{

	InitFlagMap(Map,100,100,20);       
	AddNodey0(Map,2);
	AddNodey0(Map,2);
	AddNodey0(Map,2);
	AddNodey0(Map,2);


	AddNodex0(Map,0);
	AddNodex0(Map,0);


	AddNodey0(Map,-2);
	AddNodey0(Map,-2);
	

	long int i=0;
	for(i=5;i<45;i++)
	{	
		GetNode(Map,i,40)->Kind=Obstacle;
		GetNode(Map,i,60)->Kind=Obstacle;
	}

	for(i=40;i<60;i++)
	{
		GetNode(Map,45,i)->Kind=Obstacle;
	}

	//SDL_SetRenderDrawColor(renderer,0,0,0,255);
	//SDL_RenderPresent(renderer); //Affiche le rendu sur l'écran

	
	SetLineObstacle(Map,50,20,50,70);


}


void MapUTBM(FlagMap *Map)
{
	
	InitFlagMap(Map,1,1,10);
	long int i=0,j=0;
	
	//U
	
	for(j=0;j<15;j++)
	{
		for(i=0;i<100;i++)
		{
			AddNodeyN(Map,j);
		}
		AddNodexN(Map,0);
	}

	for(j=15;j<30;j++)
	{
		for(i=0;i<30;i++)
		{
			AddNodeyN(Map,j);
		}
		AddNodexN(Map,0);
	}

	for(j=30;j<45;j++)
	{
		for(i=0;i<100;i++)
		{
			AddNodeyN(Map,j);
		}
		AddNodexN(Map,0);
	}

	//T
	
	for(j=45;j<90;j++)
	{
		for(i=70;i<100;i++)
		{
			AddNodexy(Map,j,i);
		}
	}

	for(j=60;j<75;j++)
	{
		for(i=69;i>0;i--)
		{
			AddNodexy(Map,j,i);
		}
	}
	
	//B
	
	for(j=90;j<105;j++)
	{
		for(i=99;i>0;i--)
		{
			AddNodexy(Map,j,i);
		}	
	}

	for(j=105;j<125;j++)
	{
	
		for(i=1;i<15;i++)
		{
			AddNodexy(Map,j,i);
		}

		for(i=30;i<45;i++)
		{
			AddNodexy(Map,j,i);
		}
	}

	for(j=110;j<125;j++)
	{
		for(i=15;i<30;i++)
		{
			AddNodexy(Map,j,i);
		}
	}

	//M
	
	for(j=125;j<150;j++)
	{
		for(i=1;i<40;i++)
		{
			AddNodexy(Map,j,i+3*(j-125));
		}
	
	}

	for(j=150;j<175;j++)
	{
		for(i=99;i>59;i--)
		{
			AddNodexy(Map,j,i-3*(j-150));
		}
	
	}	
	
	for(j=175;j<200;j++)
	{
		for(i=1;i<40;i++)
		{
			AddNodexy(Map,j,i+3*(j-175));
		}
	
	}

	for(j=200;j<225;j++)
	{
		for(i=99;i>59;i--)
		{
			AddNodexy(Map,j,i-3*(j-200));
		}
	
	}	
	



	/*
	//M
	for(j=90;j<135;j++)
	{
		for(i=99;i>70;i--)
		{
			AddNodexy(Map,j,i-j+90);	
		}
	}

	
	for(j=135;j<180;j++)
	{
		for(i=27;i<57;i++)
		{
			AddNodexy(Map,j,i+j-135);	
		}
	}

	*/
	
}


void DispAndWait(Screen *scr,FlagMap *Map,AList *ListH,AList *ListD)
{

	SDL_Event e;
	int ww,wh; 
	SDL_MaximizeWindow(*(scr->window));
	while(e.key.keysym.sym!=SDLK_DOWN && e.type!=SDL_QUIT)
	{
		SDL_GetWindowSize(*(scr->window),&ww,&wh);

		if(e.window.type!=SDL_WINDOWEVENT_SIZE_CHANGED&& e.type!=SDL_MOUSEMOTION)
		{


			DrawMap(scr,Map);
			DrawPath(scr,Map,ListH,GREEN);
			DrawPath(scr,Map,ListD,BLUE);
		
			SDL_SetRenderDrawColor(*(scr->renderer),0,0,255,255);
			SDL_RenderPresent(*(scr->renderer));

		
		}

		SDL_WaitEvent(&e);
	}


}
