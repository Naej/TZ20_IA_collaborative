/*********************************************************************************
*     File Name           :     State.h
*     Created By          :     robin
*     Creation Date       :     [2017-01-01 22:16]
*     Last Modified       :     [2017-01-02 00:05]
*     Description         :     Définition des structures d'état 
**********************************************************************************/

#include "Condition.h"
#include "Causality.h"


typedef struct State State;

struct State
{
	LogicTriplet*** Condition;

	
	State *Correspondence; //Ceci est un tableau d'états 

	//Nous allons juxtaposer un tableau d'état (Correspondence) à la structure FlagCausality...
	//càd que la liste FlagCausality décrit un tableau simple de type Causality qui
	//lui meme est un quintuplet.
	//Au 1er élément de Correspondence on fait correspondre le 1er quintuplet d'état etc...
	


};


