#ifndef CONDITION_H
#define CONDITION_H

/*********************************************************************************
*     File Name           :     Condition.h
*     Created By          :     robin
*     Creation Date       :     [2016-12-16 15:41]
*     Last Modified       :     [2017-01-01 02:15]
*     Description         :      
**********************************************************************************/





//Matrice de triplets 


typedef struct LogicTriplet LogicTriplet;

struct LogicTriplet
{
	char *Triplet; //On conviendra l'indice 0 = Et/Ou/OuExcl
		       //  			1 = >/>=/</<=...
		       //  			2 = Booleen (Condition vraie/fausse ?)

};

void InitMatrix(LogicTriplet***,short unsigned int n);



//Fonctions pour gérer le tableau 2D de triplet

void LTAddLine(LogicTriplet***);
void CreateMatrix(LogicTriplet ***,short unsigned int Dimension); //Cesont des matrices carrées

void DeleteMatrix(LogicTriplet	***,short unsigned int n);

//Renvois le booleen associé à la condition de coordonnés (i,j)
char GetValueMatrix(LogicTriplet ***,short unsigned int i,short unsigned int j); //Le triple pointeur n'est pas nécessaire ici mais s'accomode mieux au reste des fonctions


#endif /* CONDITION_H */
