/*********************************************************************************
*     File Name           :     Mesh.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-13 17:39]
*     Last Modified       :     [2017-01-14 20:23]
*     Description         :     Fonctions pour la liste chainée FlagMap 
**********************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include "Mesh.h"


void DebugMap(FlagMap *Map,char dir,long int index) //Affiche l'adressage d'une ligne 'l' ou d'une colone 'c'
{
	Node *Cursor=Map->Node0;
	unsigned long int i=0;
	if(Cursor!=NULL)
	{

		if(dir=='c')
		{
			if(Cursor->NextNodey!=NULL)
			{
				for(i=0;i<index;i++)
				{
					Cursor=Cursor->NextNodex;
					//printf("###%p###",Cursor->NextNodex);
				}
				
				while(Cursor->PrevNodey!=NULL)
				{
					Cursor=Cursor->PrevNodey;
				}


				do
				{

					printf("(%2lu,%2lu) (%2.1lf,%2.1lf) Current:\t%9p\txPrev:\t%9p\txNext:\t%9p\tyPrev:\t%9p\tyNext:\t%9p\n",Cursor->Gridx,Cursor->Gridy,Cursor->x,Cursor->y,Cursor,Cursor->PrevNodex,Cursor->NextNodex,Cursor->PrevNodey,Cursor->NextNodey);

					Cursor=Cursor->NextNodey;
				}while(Cursor!=NULL);
			}
			else
			{
				printf("###WARNING### Nothing to print\n");
			}

		}
		else if(dir=='l')
		{
			if(Cursor->NextNodex!=NULL)
			{
				
				for(i=0;i<index;i++)
				{
					Cursor=Cursor->NextNodey;
				
				}

				while(Cursor->PrevNodex!=NULL)
				{
					Cursor=Cursor->PrevNodex;
				}
				

				do
				{

					printf("(%2lu,%2lu) (%2.1lf,%2.1lf) Current:\t%9p\txPrev:\t%9p\txNext:\t%9p\tyPrev:\t%9p\tyNext:\t%9p\n",Cursor->Gridx,Cursor->Gridy,Cursor->x,Cursor->y,Cursor,Cursor->PrevNodex,Cursor->NextNodex,Cursor->PrevNodey,Cursor->NextNodey);

					Cursor=Cursor->NextNodex;
				}while(Cursor!=NULL);
			}
			else
			{
				printf("###WARNING### Nothing to print\n");
			}

		}

	}
	else
	{
		printf("###WARNING### Nothing to print\n");
	}
	

}

void DebugMapZ(FlagMap *Map)
{
	Node *Cursor=Map->Node0;
	do
	{
		printf("(%2ld,%2ld) (%2.1lf,%2.1lf) Current:\t%9p\txPrev:\t%9p\txNext:\t%9p\tyPrev:\t%9p\tyNext:\t%9p\n",Cursor->Gridx,Cursor->Gridy,Cursor->x,Cursor->y,Cursor,Cursor->PrevNodex,Cursor->NextNodex,Cursor->PrevNodey,Cursor->NextNodey);
		Cursor=Cursor->NextNodez;
	}while(Cursor!=NULL);

}


void AddNodexy(FlagMap *Map,long int x, long int y) //OK
{
	if(GetNode(Map,x,y)!=NULL)
	{
		printf("###ERROR: AddNodexy (%lu,%lu) is not on the edge\n",x,y);
		exit(EXIT_FAILURE);
	}
	Node *Cursor=Map->Node0;
	Node **Tab=(Node**)malloc(4*sizeof(Node*));
	Tab[0]=NULL;
	Tab[1]=NULL;
	Tab[2]=NULL;
	Tab[3]=NULL;
	int IndexTab=0;
	int i=0;
	while(Cursor!=NULL)
	{
		if((Cursor->Gridx-x)*(Cursor->Gridx-x)+(Cursor->Gridy-y)*(Cursor->Gridy-y)==1)
		{
			Tab[IndexTab]=Cursor;
			IndexTab++;
		}
		Cursor=Cursor->NextNodez;
	}

	if(IndexTab==0)
	{
		printf("###ERROR: AddNodexy (%lu,%lu) is not on the edge\n",x,y);
		exit(EXIT_FAILURE);
	}
	
	Node *Temp=(Node*)malloc(sizeof(Node));
	for(i=0;Tab[i]!=NULL;i++) //Pour chaque élément
	{
		if(Tab[i]->Gridx<x)
		{
			Tab[i]->NextNodex=Temp;
			Temp->PrevNodex=Tab[i];
		}
		if(Tab[i]->Gridx>x)
		{
			Tab[i]->PrevNodex=Temp;
			Temp->NextNodex=Tab[i];
		}
		if(Tab[i]->Gridy<y)
		{
			Tab[i]->NextNodey=Temp;
			Temp->PrevNodey=Tab[i];
		}
		if(Tab[i]->Gridy>y)
		{
			Tab[i]->PrevNodey=Temp;
			Temp->NextNodey=Tab[i];
		}
	}

	Temp->Kind=Path;

	Temp->Gridx=x;
	Temp->Gridy=y;
	Temp->x=x*Map->Gap;
	Temp->y=y*Map->Gap;

	Map->LastNode->NextNodez=Temp;
	Temp->PrevNodez=Map->LastNode;
	Temp->NextNodez=NULL;
	Map->LastNode=Temp;

	if(x<Map->GridxMin)
	{
		Map->GridxMin=x;
	}
	
	if(x>Map->GridxMax)
	{
		Map->GridxMax=x;
	}
	
	if(y<Map->GridyMin)
	{
		Map->GridyMin=y;
	}
	
	if(y>Map->GridyMax)
	{
		Map->GridyMax=y;
	}
	
	free(Tab);
	Tab=NULL;
}

void AddNodex0(FlagMap *Map,long int index)
{
	Node *Cursor=Map->Node0;

	long int i=0;

	Node *CursorP=NULL;
	Node *CursorM=NULL;


	for(i=0;i<index;i++)
	{
		Cursor=Cursor->NextNodey;
	}

	for(i=0;i>index;i--)
	{
		Cursor=Cursor->PrevNodey;
	}

	CursorP=Cursor->NextNodey;
	CursorM=Cursor->PrevNodey;
	i=0;
	while(Cursor->PrevNodex!=NULL)
	{
		i--;
		Cursor=Cursor->PrevNodex;
		if(CursorP!=NULL)
		{
			CursorP=CursorP->PrevNodex;
		}

		if(CursorM!=NULL)
		{
			CursorM=CursorM->PrevNodex;
		}
	}	


	Cursor->PrevNodex=(Node*)malloc(sizeof(Node));
	if(Cursor->PrevNodex==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}

	Cursor->PrevNodex->Kind=Path;	
	Cursor->PrevNodex->NextNodex=Cursor;
	Cursor->PrevNodex->PrevNodex=NULL;
	
	if(CursorP!=NULL) //Si les noeuds sont aux mêmes "niveaux"
	{
		if(CursorP->PrevNodex!=NULL)
		{
			Cursor->PrevNodex->NextNodey=CursorP->PrevNodex;
			CursorP->PrevNodex->PrevNodey=Cursor->PrevNodex;
		}
		else
		{
			Cursor->PrevNodex->NextNodey=NULL;
		}
		
	}
	else
	{
		Cursor->PrevNodex->NextNodey=NULL;
	}

	if(CursorM!=NULL)
	{
		if(CursorM->PrevNodex!=NULL)
		{
			Cursor->PrevNodex->PrevNodey=CursorM->PrevNodex;
			CursorM->PrevNodex->NextNodey=Cursor->PrevNodex;
		}
		else
		{
			Cursor->PrevNodex->PrevNodey=NULL;
		}
	}
	else
	{
		Cursor->PrevNodex->PrevNodey=NULL;
	}

	Cursor->PrevNodex->Gridx=i-1;
	Cursor->PrevNodex->Gridy=index;
	Cursor->PrevNodex->x=(i-1)*Map->Gap;
	Cursor->PrevNodex->y=index*Map->Gap;
	
	//Fonctions utiles à l'affichage (Pour éviter le dépassement d'écran)
	if(index<Map->GridyMin)
	{
		Map->GridyMin=index;
	}

	if(i-1<Map->GridxMin)
	{
		Map->GridxMin=i-1;
	}
	
	//Optimisation
	Map->LastNode->NextNodez=Cursor->PrevNodex;
	Cursor->PrevNodex->PrevNodez=Map->LastNode;
	Cursor->PrevNodex->NextNodez=NULL;
	Map->LastNode=Cursor->PrevNodex;
}

void AddNodey0(FlagMap *Map,long int index)
{
	Node *Cursor=Map->Node0;

	long int i=0;

	Node *CursorP=NULL;
	Node *CursorM=NULL;


	for(i=0;i<index;i++)
	{
		Cursor=Cursor->NextNodex;
	}

	for(i=0;i>index;i--)
	{
		Cursor=Cursor->PrevNodex;
	}

	CursorP=Cursor->NextNodex;
	CursorM=Cursor->PrevNodex;
	i=0;
	while(Cursor->PrevNodey!=NULL)
	{
		i--;
		Cursor=Cursor->PrevNodey;
		if(CursorP!=NULL)
		{
			CursorP=CursorP->PrevNodey;
		}

		if(CursorM!=NULL)
		{
			CursorM=CursorM->PrevNodey;
		}
	}	


	Cursor->PrevNodey=(Node*)malloc(sizeof(Node));
	if(Cursor->PrevNodey==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}

	Cursor->PrevNodey->Kind=Path;	
	Cursor->PrevNodey->NextNodey=Cursor;
	Cursor->PrevNodey->PrevNodey=NULL;
	
	if(CursorP!=NULL) //Si les noeuds sont aux mêmes "niveaux"
	{
		if(CursorP->PrevNodey!=NULL)
		{
			Cursor->PrevNodey->NextNodex=CursorP->PrevNodey;
			CursorP->PrevNodey->PrevNodex=Cursor->PrevNodey;
		}
		else
		{
			Cursor->PrevNodey->NextNodex=NULL;
		}
		
	}
	else
	{
		Cursor->PrevNodey->NextNodey=NULL;
	}

	if(CursorM!=NULL)
	{
		if(CursorM->PrevNodey!=NULL)
		{
			Cursor->PrevNodey->PrevNodex=CursorM->PrevNodey;
			CursorM->PrevNodey->NextNodex=Cursor->PrevNodey;
		}
		else
		{
			Cursor->PrevNodey->PrevNodex=NULL;
		}
	}
	else
	{
		Cursor->PrevNodey->PrevNodey=NULL;
	}

	Cursor->PrevNodey->Gridy=i-1;
	Cursor->PrevNodey->Gridx=index;
	Cursor->PrevNodey->y=(i-1)*Map->Gap;
	Cursor->PrevNodey->x=index*Map->Gap;
	
	//Fonctions utiles à l'affichage (Pour éviter le dépassement d'écran)
	if(index<Map->GridxMin)
	{
		Map->GridxMin=index;
	}

	if(i-1<Map->GridyMin)
	{
		Map->GridyMin=i-1;
	}
	//Optimisation
	Map->LastNode->NextNodez=Cursor->PrevNodey;
	Cursor->PrevNodey->PrevNodez=Map->LastNode;
	Cursor->PrevNodey->NextNodez=NULL;
	Map->LastNode=Cursor->PrevNodey;
}




void AddNodexN(FlagMap *Map,long int index) //Ajouter un noeud à la fin de Map sur l'axe x
{	
	Node *Cursor=Map->Node0;

	unsigned long int i=0;

	Node *CursorP=NULL;
	Node *CursorM=NULL;


	for(i=0;i<index;i++)
	{
		Cursor=Cursor->NextNodey;
	}

	for(i=0;i>index;i--)
	{
		Cursor=Cursor->PrevNodey;
	}

	CursorP=Cursor->NextNodey;
	CursorM=Cursor->PrevNodey;
	i=0;
	while(Cursor->NextNodex!=NULL)
	{
		i++;
		Cursor=Cursor->NextNodex;
		if(CursorP!=NULL)
		{
			CursorP=CursorP->NextNodex;
		}

		if(CursorM!=NULL)
		{
			CursorM=CursorM->NextNodex;
		}
	}	


	Cursor->NextNodex=(Node*)malloc(sizeof(Node));
	
	if(Cursor->NextNodex==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}

	Cursor->NextNodex->Kind=Path;	
	Cursor->NextNodex->PrevNodex=Cursor;
	Cursor->NextNodex->NextNodex=NULL;
	
	if(CursorP!=NULL) //Si les noeuds sont aux mêmes "niveaux"
	{
		if(CursorP->NextNodex!=NULL)
		{
			Cursor->NextNodex->NextNodey=CursorP->NextNodex;
			CursorP->NextNodex->PrevNodey=Cursor->NextNodex;
		}
		else
		{
			Cursor->NextNodex->NextNodey=NULL;
		}
		
	}
	else
	{
		Cursor->NextNodex->NextNodey=NULL;
	}

	if(CursorM!=NULL)
	{
		if(CursorM->NextNodex!=NULL)
		{
			Cursor->NextNodex->PrevNodey=CursorM->NextNodex;
			CursorM->NextNodex->NextNodey=Cursor->NextNodex;
		}
		else
		{
			Cursor->NextNodex->PrevNodey=NULL;
		}
	}
	else
	{
		Cursor->NextNodex->PrevNodey=NULL;
	}

	Cursor->NextNodex->Gridx=i+1;
	Cursor->NextNodex->Gridy=index;
	Cursor->NextNodex->x=(i+1)*Map->Gap;
	Cursor->NextNodex->y=index*Map->Gap;
	
	//Fonctions utiles à l'affichage (Pour éviter le dépassement d'écran)
	if(index>Map->GridyMax)
	{
		Map->GridyMax=index;
	}

	if(i+1>Map->GridxMax)
	{
		Map->GridxMax=(long int)i+1;
	}
		

	// Permet de réduire la compléxité
	// La compléxité du code pourra éventuellement être réduite à d'autres 
	// endroits de la même façon
	
	Map->LastNode->NextNodez=Cursor->NextNodex;
	Cursor->NextNodex->PrevNodez=Map->LastNode;
	Cursor->NextNodex->NextNodez=NULL;
	Map->LastNode=Cursor->NextNodex;
		
}

void AddNodeyN(FlagMap *Map,long int index)
{
	Node *Cursor=Map->Node0;

	unsigned long int i=0;

	Node *CursorP=NULL;
	Node *CursorM=NULL;


	for(i=0;i<index;i++)
	{
		Cursor=Cursor->NextNodex;
	}

	for(i=0;i>index;i--)
	{
		Cursor=Cursor->PrevNodex;
	}

	CursorP=Cursor->NextNodex;
	CursorM=Cursor->PrevNodex;
	i=0;
	while(Cursor->NextNodey!=NULL)
	{
		i++;
		Cursor=Cursor->NextNodey;
		if(CursorP!=NULL)
		{
			CursorP=CursorP->NextNodey;
		}

		if(CursorM!=NULL)
		{
			CursorM=CursorM->NextNodey;
		}
	}	


	Cursor->NextNodey=(Node*)malloc(sizeof(Node));
	
	if(Cursor->NextNodey==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}
	Cursor->NextNodey->Kind=Path;	
	Cursor->NextNodey->PrevNodey=Cursor;
	Cursor->NextNodey->NextNodey=NULL;
	
	if(CursorP!=NULL) //Si les noeuds sont aux mêmes "niveaux"
	{
		if(CursorP->NextNodey!=NULL)
		{
			Cursor->NextNodey->NextNodex=CursorP->NextNodey;
			CursorP->NextNodey->PrevNodex=Cursor->NextNodey;
		}
		else
		{
			Cursor->NextNodey->NextNodex=NULL;
		}
		
	}
	else
	{
		Cursor->NextNodey->NextNodex=NULL;
	}

	if(CursorM!=NULL)
	{
		if(CursorM->NextNodey!=NULL)
		{
			Cursor->NextNodey->PrevNodex=CursorM->NextNodey;
			CursorM->NextNodey->NextNodex=Cursor->NextNodey;
		}
		else
		{
			Cursor->NextNodey->PrevNodex=NULL;
		}
	}
	else
	{
		Cursor->NextNodey->PrevNodex=NULL;
	}
	

	Cursor->NextNodey->Gridx=index;
	Cursor->NextNodey->Gridy=i+1;
	Cursor->NextNodey->x=index*Map->Gap;
	Cursor->NextNodey->y=(i+1)*Map->Gap;


	//Fonctions utiles à l'affichage (Pour éviter le dépassement d'écran)
	if(index>Map->GridxMax)
	{
		Map->GridxMax=(long unsigned int)index;
	}

	if(i+1>Map->GridyMax)
	{
		Map->GridyMax=i+1;
	}
	
	Map->LastNode->NextNodez=Cursor->NextNodey;
	Cursor->NextNodey->PrevNodez=Map->LastNode;
	Cursor->NextNodey->NextNodez=NULL;
	Map->LastNode=Cursor->NextNodey;

}



// OK 
void InitFlagMap(FlagMap *Map,long int n,long int p,float Gap)
{
	
	Map->Node0=(Node*)malloc(sizeof(Node));
	Map->Node0->Kind=Path;
	
	if(Map->Node0==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}
	
	Map->Gap=Gap;	
	Map->Node0->PrevNodex=NULL;
	Map->Node0->PrevNodey=NULL;
		
	Map->Node0->NextNodex=NULL;
	Map->Node0->NextNodey=NULL;
	

	Map->Node0->NextNodez=NULL;
	Map->Node0->PrevNodez=NULL;
	
	Map->LastNode=Map->Node0;

	Map->Node0->x=0;
	Map->Node0->y=0;
	Map->Node0->Gridx=0;
	Map->Node0->Gridy=0;

	
	Map->GridxMax=0;
	Map->GridyMax=0;
	Map->GridxMin=0;
	Map->GridyMin=0;	


	unsigned long int i=0,j=0;
		
	//Les deux premiers for servent à définir les contours de la map	
	for(i=0;i<n-1;i++) //n-1 car on veut une map de dim nxp or l'élement 0 existe toujours
	{
		AddNodeyN(Map,0);
	}
	
	
	
	for(i=0;i<p-1;i++)
	{
		AddNodexN(Map,0);
	}
	
		

	//Tandis que ce for sert à remplir la map avec les noeuds
	
	
	for(i=1;i<p;i++)
	{
		for(j=1;j<n;j++)
		{
			AddNodeyN(Map,i);
		}
	}
	
	DebugMapZ(Map);
}


void DeleteFlagMap(FlagMap *Map)
{
	Node *Cursor=Map->Node0;
	while(Cursor!=NULL)
	{
		if(Cursor->PrevNodez!=NULL)
		{
			free(Cursor->PrevNodez);
			Cursor->PrevNodez=NULL;
		}
		
		if(Cursor->NextNodez!=NULL)
		{
			Cursor=Cursor->NextNodez;
		}
		else 
		{
			free(Cursor);
			Cursor=NULL;
			break; // Je sais, c'est sale, mais ça marche, c'était pour voir d'où venait la fuite mémoire 
		}
	}
	
	free(Map);
	Map=NULL;
}

Node *GetNode(FlagMap *Map,long int x,long int y)
{
// Avec optimisation
	Node *Cursor=Map->Node0;

	while(Cursor->Gridx!=x || Cursor->Gridy!=y)
	{
		Cursor=Cursor->NextNodez;
		
		if(Cursor==NULL)
		{
			break;
		}
	}
	

// sans optimisation

/*	
	long int i=0;
	Node *Cursor=Map->Node0;
	
	
	
	for(i=0;i<x;i++)
	{
		Cursor=Cursor->NextNodex;
		if(Cursor==NULL)
		{
			printf("###WARNING: The index column %ld does't exist\nThe function returned NULL\n",x);
			return NULL;
		}
	}
	
	for(i=0;i>x;i--)
	{
		Cursor=Cursor->PrevNodex;
		if(Cursor==NULL)
		{
			printf("###WARNING: The index column %ld does't exist\nThe function returned NULL\n",x);
			return NULL;
		}
	}

	for(i=0;i<y;i++)
	{
		Cursor=Cursor->NextNodey;
		if(Cursor==NULL)
		{
			printf("###WARNING: The index line %ld does't exist\nThe function returned NULL\n",y);
			return NULL;
		}
	}

	for(i=0;i>y;i--)
	{
		Cursor=Cursor->PrevNodey;
		if(Cursor==NULL)
		{
			printf("###WARNING: The index line %ld does't exist\nThe function returned NULL\n",y);
			return NULL;
		}
	}

*/	


	


	return Cursor;
}



//Non testé
Node *GetNearestNode(FlagMap *Map,double x,double y) //Renvois le noeud le plus proche des coordonnés indiquées
{
	// Remarque cette fonction devient caduque si le maillage est très dense
	 
	Node *Cursor=Map->Node0;
	
	while(Cursor->x<x)
	{
		Cursor->NextNodex;
		if(Cursor->NextNodex==NULL)
		{
			break;
		}
	}
	
	if(Cursor->x-x>x-Cursor->PrevNodex->x) //Détermination du plus proche noeud sur x
	{
		Cursor=Cursor->PrevNodex;
	}
	
	while(Cursor->y<y)
	{
		Cursor->NextNodey;
		if(Cursor->NextNodey==NULL)
		{
			break;
		}
	}

	if(Cursor->y-y>y-Cursor->PrevNodey->y)
	{
		Cursor=Cursor->PrevNodey;
	}

}


Node *GetApproxNearestNode(FlagMap *Map,double x,double y) //Non achevé
{
	Node *Cursor=Map->Node0;

	double i=0;

	for(i=0.;i<Cursor->x;i+=Map->Gap)
	{
		Cursor=Cursor->NextNodex;
	}

	for(i=0.;i<Cursor->y;i+=Map->Gap)
	{
		Cursor=Cursor->NextNodey;
	}

	return Cursor;
}

