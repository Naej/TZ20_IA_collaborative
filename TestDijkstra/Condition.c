/*********************************************************************************
*     File Name           :     Condition.c
*     Created By          :     robin
*     Creation Date       :     [2016-12-21 21:51]
*     Last Modified       :     [2017-01-01 02:16]
*     Description         :      
**********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include "Condition.h"



void CreateMatrix(LogicTriplet ***Matrix,short unsigned int n) //Tests OK
{
	(*Matrix)=(LogicTriplet**)calloc(n,sizeof(LogicTriplet *));
	short unsigned int i=0,j=0;


	if(*Matrix==NULL)
	{
		printf("###ERROR Calloc ###\n");
		exit(EXIT_FAILURE);
	}

	for(i=0;i<n;i++)
	{
		(*Matrix)[i]=(LogicTriplet*)calloc(n,sizeof(LogicTriplet));
		
		if((*Matrix)[i]==NULL)
		{
			printf("###ERROR Calloc ###\n");
			exit(EXIT_FAILURE);
		}

	}
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{


			((*Matrix)[i][j]).Triplet=(char*)calloc(3,sizeof(char));
			if((((*Matrix)[i][j]).Triplet)==NULL)
			{
				printf("###ERROR Calloc ###\n");
				exit(EXIT_FAILURE);
			}

		}
	}
	
}


void DeleteMatrix(LogicTriplet ***Matrix,short unsigned int n) //OK pas de fuite mémoire d'après valgrind
{
	short unsigned int i=0,j=0;
	
	for(i=0;i<n;i++)
	{
		for(j=0;j<n;j++)
		{
			free(((*Matrix)[i][j]).Triplet);
		}
	}
	
	
	for(i=0;i<n;i++)
	{
		free((*Matrix)[i]);
		(*Matrix)[i]=NULL;
	}

	free(*Matrix);
	*Matrix=NULL;
}

char GetValueMatrix(LogicTriplet*** Matrix,short unsigned int i,short unsigned int j)
{
	return ((*Matrix)[i][j].Triplet)[2];
}

