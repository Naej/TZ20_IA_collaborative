#ifndef DRAW_H
#define DRAW_H

/*********************************************************************************
*     File Name           :     Draw.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-09 14:07]
*     Last Modified       :     [2017-01-12 02:37]
*     Description         :     Déscription des fonctions de déssin 
**********************************************************************************/

#include <SDL2/SDL.h>
#include "Color.h"
#include "Screen.h"
#include "Mesh.h"
#include "AStar.h"

void Line(Screen*,int,int,int,int,Color);
void FilledRect(Screen*,int,int,int,int,Color);
void FilledCircle(Screen*,int,int,int,Color);

void DrawGridPoint(Screen*,FlagMap*,int Gridx ,int Gridy); // Afficher un point du maillage en prenant en compte les dimensions de la fenêtre


void DrawGridLine(Screen *scr,FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2);


void DrawMap(Screen*,FlagMap*); // => Map rectangulaire, Coord +

void DrawPath(Screen*,FlagMap*,AList*,Color);
#endif /* DRAW_H */
