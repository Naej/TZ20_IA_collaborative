#ifndef SCREEN_H
#define SCREEN_H

/*********************************************************************************
*     File Name           :     Screen.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-09 14:55]
*     Last Modified       :     [2016-10-09 16:28]
*     Description         :     La structure Screen sert à rassembler les structures
*     				SDL_Window et SDL_Renderer dans une même structure
*     				pour éviter toute ambiguïté lors d'appel de fonctions
*     				nécessitant tantot l'un tantot l'autre ou nécessitant
*     				les deux à la fois
**********************************************************************************/
typedef struct Screen Screen;

struct Screen
{
	SDL_Window **window;
	SDL_Renderer **renderer;
};


#endif /* SCREEN_H */
