/*********************************************************************************
*     File Name           :     Color.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-09 03:10]
*     Last Modified       :     [2016-10-09 03:17]
*     Description         :     Fonctions de la structure Color 
**********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include "Color.h"



Color GetColor(uint8_t R,uint8_t G,uint8_t B,uint8_t Alpha)
{
	Color lColor;
	lColor.Red=R;
	lColor.Green=G;
	lColor.Blue=B;
	lColor.Alpha=Alpha;
	return lColor;
}


void SetColor(Color *Clr,uint8_t R,uint8_t G,uint8_t B,uint8_t Alpha)
{
	Clr->Red=R;
	Clr->Green=G;
	Clr->Blue=B;
	Clr->Alpha=Alpha;
}
