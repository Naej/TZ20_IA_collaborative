/*********************************************************************************
*     File Name           :     MathSpec.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-30 01:29]
*     Last Modified       :     [2016-12-21 18:24]
*     Description         :     Fonctions de maths quelconques 
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "MathSpec.h"


double Range(double Ymax,double Ymin,double Xmax,double Xmin,double X) // Lorsque X varie de Xmin à Xmax, Range varie proportionnellement de Ymin à Ymax
{
	return (Ymax-Ymin)/(Xmax-Xmin)*(X-Xmin)+Ymin;
}


/*
void InitMatrix(Matrix* A)
{
	A=(Matrix*)malloc(sizeof(Matrix));
	A->Column=NULL;
	A->Line=NULL;
	A->LineMax=0;
	A->ColumnMax=0;
}

void MatrixAddLine(Matrix *A)
{
	(A->LineMax)++;
	if(A->Line==NULL)
	{
		A->Line=(float*)malloc(sizeof(float));
		(A->Line)[0]=0.f;
	}
	else
	{
		float *Temp=(float*)realloc(A->Line,A->LineMax*sizeof(float));

		if(Temp==NULL)
		{
			printf("###realloc error ###\n");
			free(A->Line);
			free(A->Column);
			exit(EXIT_FAILURE);
		}
		else
		{
			A->Line=Temp;
			(A->Line)[A->LineMax-1]=0.f;
		}
	}
}


void MatrixAddColumn(Matrix *A)
{
	(A->ColumnMax)++;
	if(A->Column==NULL)
	{
		A->Column=(float*)malloc(sizeof(float));
		(A->Column)[0]=0.f;
	}
	else
	{
		float *Temp=(float*)realloc(A->Column,A->ColumnMax*sizeof(float));

		if(Temp==NULL)
		{
			printf("###realloc error ###\n");
			free(A->Column);
			free(A->Line);
			exit(EXIT_FAILURE);
		}
		else
		{
			A->Column=Temp;
			(A->Column)[A->ColumnMax-1]=0.f;
		}
	}
}

void MatrixDeleteLine(Matrix*)

*/
