#ifndef COLOR_H
#define COLOR_H

/*********************************************************************************
*     File Name           :     Color.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-09 02:47]
*     Last Modified       :     [2016-10-09 16:06]
*     Description         :     Définition structure Color 
**********************************************************************************/


//Couleurs



typedef struct Color Color;
struct Color
{
	uint8_t Red;
	uint8_t Green;
	uint8_t Blue;
	uint8_t Alpha;
};

Color GetColor(uint8_t,uint8_t,uint8_t,uint8_t);

#define RED   GetColor(255, 0 , 0 ,255)
#define GREEN GetColor( 0 ,255, 0 ,255)
#define BLUE  GetColor( 0 , 0 ,255,255)
#define DARK  GetColor( 0 , 0 , 0 ,255)
#define WHITE GetColor(255,255,255,255)




void SetColor(Color*,uint8_t,uint8_t,uint8_t,uint8_t);

#endif /* COLOR_H */
