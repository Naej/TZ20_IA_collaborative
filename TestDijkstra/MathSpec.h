#ifndef MATHSPEC_H
#define MATHSPEC_H

/*********************************************************************************
*     File Name           :     MathSpec.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-30 01:23]
*     Last Modified       :     [2016-12-21 18:25]
*     Description         :     Ensemble de fonctions de maths quelconques 
**********************************************************************************/


double Range(double Ymax,double Ymin,double Xmax,double Xmin,double X);


/*
typedef struct Matrix Matrix;



struct Matrix
{
	float *Column;
	float *Line;

	unsigned int LineMax;
	unsigned int ColumnMax;
};

void InitMatrix(Matrix*);
void MatrixAddLine(Matrix*);
void MatrixAddColumn(Matrix*);
void MatrixDeleteLine(Matrix*,unsigned int *index);
void MatrixDeleteColumn(Matrix*,unsigned int *index);
void DeleteMatrix(Matrix*);
*/

#endif /* MATHSPEC_H */
