#ifndef MESH_H
#define MESH_H

/*********************************************************************************
*     File Name           :     mesh.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-13 10:14]
*     Last Modified       :     [2017-01-14 20:23]
*     Description         :     Déscription liste chainé 2D  
**********************************************************************************/

enum NodeKind {Path=0,Robot=1,Obstacle=2,Target=3,Fog=4,EndZone=5}; 
typedef enum NodeKind NodeKind;


typedef struct Node Node;


struct Node
{
	Node *NextNodex;
	Node *NextNodey;
	Node *PrevNodex;
	Node *PrevNodey;
	
	Node *NextNodez; //Sert à simplifier,accélerer les fonctions d'affichage
	Node *PrevNodez;

	double x,y;//Vrais Coord càd coordonnés dont la valeur traduisent une longueur
	long int Gridx,Gridy; //Coordonnés relatifs au maillage.
	
	
	float Radius,AbsoluteAngle;

	NodeKind Kind;

};


struct FlagMap // Définition d'un ensemble de noeud de dimension nxp
{
	Node *Node0;
	float Gap; //écart en mètre entre deux noeud

	//Utile à l'affichage
	Node *LastNode; //Dernier noeud crée sur z, permet d'éviter des boucles	
	long int GridxMax,GridyMax,GridxMin,GridyMin;
};

typedef struct FlagMap FlagMap;

void DebugMap(FlagMap *,char dir,long int index);
void DebugMapZ(FlagMap*);

void InitFlagMap(FlagMap*Map,long int n,long int p,float Gap);
void DeleteFlagMap(FlagMap *);

// Créer une Map rectangulaire à coord + 
// Map est la structure de controle du maillage
// n est le nombre de lignes
// p est le nombre de colones
// Gap est l'espace laissé en 'm' entre chaque noeud

Node *GetNode(FlagMap*,long int x,long int y); //Renvois le noeud du couple (x,y)


void AddNodexy(FlagMap *M,long int x,long int y);// Si le point (x,y) est accessible depuis un bord, un noeud est crée à la position x,y

void AddNodex0(FlagMap *M,long int index); //Ajout d'un noeud au début de la ligne index
void AddNodey0(FlagMap *M,long int index); //Ajout d'un noeud au début de la colone index

void AddNodexN(FlagMap *M,long int index); //Ajout d'un noeud à la fin de la ligne index
void AddNodeyN(FlagMap *M,long int index); //Ajout d'un noeud à la fin de la colone index
void DelFlagMap(FlagMap *M); //Supprime le maillage M

#endif /* MESH_H */
