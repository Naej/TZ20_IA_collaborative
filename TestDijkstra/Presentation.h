#ifndef PRESENTATION_H
#define PRESENTATION_H
#include "AStar.h"
#include "SDL2/SDL.h"
#include "Screen.h"
#include "Mesh.h"

/*********************************************************************************
*     File Name           :     Presentation.h
*     Created By          :     robin
*     Creation Date       :     [2017-01-11 20:18]
*     Last Modified       :     [2017-01-11 22:30]
*     Description         :     Ensemble de fonction pour la présentation du projet uniquement 
**********************************************************************************/



void SetLineObstacle(FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2 );


// Le 1er point spécifié (Gridx1,Gridy1) doit exister
// Non achevé
void AddNodeLine(FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2);

void MapUTBM(FlagMap *Map);
void MapU(FlagMap *Map);

void DispAndWait(Screen *scr,FlagMap *Map,AList* ListH,AList *ListD);

#endif /* PRESENTATION_H */
