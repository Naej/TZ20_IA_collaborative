/*********************************************************************************
*     File Name           :     AStar.c
*     Created By          :     robin
*     Creation Date       :     [2016-11-16 23:30]
*     Last Modified       :     [2017-01-12 03:05]
*     Description         :     Fonctions utile au PathFinding A* 
**********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include "AStar.h"
#include <math.h>


void InitAList(AList *List,Node *FirstNode,double (*g)(Node*,Node*),double (*h)(Node*,Node*))
{
	List->Opened0=(ANode*)malloc(sizeof(ANode));
	if(List->Opened0==NULL)
	{
		printf("###ERROR: malloc\n");
		exit(EXIT_FAILURE);
	}
	
	List->Opened0->A_Node=FirstNode;
	List->Opened0->NextNode=NULL;
	List->OpenedN=List->Opened0;
	List->OpenedN->A_Node=FirstNode;	
	List->OpenedN->NextNode=NULL;


	List->Closed0=NULL;
	List->ClosedN=NULL;

	List->Path0=NULL;
	List->PathN=NULL;

	List->g=g;
	List->h=h;
}

void AddNodeOpenedList(AList *List,Node *A)//Ajoute un elmt à la fin de l'opened list
{
	ANode *Temp=List->Opened0;

	if(Temp==NULL)
	{
		Temp=(ANode*)malloc(sizeof(ANode));
		if(Temp==NULL)
		{
			printf("###ERROR: Malloc\n");
			exit(EXIT_FAILURE);
		}
		
		Temp->NextNode=NULL;
		Temp->A_Node=A;
		List->OpenedN=Temp;
		List->Opened0=Temp;
	}
	else
	{

		while(Temp->NextNode!=NULL)
		{
			Temp=Temp->NextNode;
		}
		Temp->NextNode=(ANode*)malloc(sizeof(ANode));
		Temp->NextNode->A_Node=A;
		Temp->NextNode->NextNode=NULL;

		List->OpenedN=Temp->NextNode;
	}

}

void AddNodeClosedList(AList *List,Node *A) //Ajoute un elmt à la fin de la closedlist
{
	ANode *Temp=List->Closed0;

	if(Temp==NULL)
	{
		Temp=(ANode*)malloc(sizeof(ANode));
		if(Temp==NULL)
		{
			printf("###ERROR: Malloc\n");
			exit(EXIT_FAILURE);
		}
		
		Temp->NextNode=NULL;
		Temp->A_Node=A;
		List->ClosedN=Temp;
		List->Closed0=Temp;
	}
	else
	{

		while(Temp->NextNode!=NULL)
		{
			Temp=Temp->NextNode;
		}
		Temp->NextNode=(ANode*)malloc(sizeof(ANode));
		Temp->NextNode->A_Node=A;
		Temp->NextNode->NextNode=NULL;

		List->ClosedN=Temp->NextNode;
	}
}


void AddNodePath(AList *List,Node *A)
{	
	ANode *Temp=List->Path0;

	if(Temp==NULL)
	{
		Temp=(ANode*)malloc(sizeof(ANode));
		Temp->NextNode=NULL;
		Temp->A_Node=A;
		List->PathN=Temp;
		List->Path0=Temp;
	}
	else
	{


		while(Temp->NextNode!=NULL)
		{
			Temp=Temp->NextNode;
		}

		Temp->NextNode=(ANode*)malloc(sizeof(ANode));
		Temp->NextNode->A_Node=A;
		Temp->NextNode->NextNode=NULL;

		List->PathN=Temp->NextNode;
	}
}



void DelNodeOpenedList(AList *List,Node *A)
{
	ANode *Temp=List->Opened0;
	ANode *NextTemp=List->Opened0;

	if(NextTemp->NextNode!=NULL) //Si la liste ne contient pas qu'un seul élement
	{
		NextTemp=Temp->NextNode;
	}

	if(A!=List->Opened0->A_Node)
	{
		while(NextTemp->A_Node!=A)
		{
			NextTemp=NextTemp->NextNode;
			Temp=Temp->NextNode;
		}
		
		
		if(A==List->OpenedN->A_Node) // ~ NextTemp==List->OpenedN
		{
			List->OpenedN=Temp;
		}

		Temp->NextNode=NextTemp->NextNode;
		NextTemp->NextNode=NULL;
		free(NextTemp);


	}
	else
	{
		List->Opened0=Temp->NextNode;
		free(Temp);
	}

}

/*
void DelNodeOpenedList(AList *List,Node *A)
{
	ANode *Temp=List->Opened0;
	ANode *NextTemp=Temp;
	
	if(A!=List->Opened0->A_Node)
	{
		while(Temp!=NULL)
		{
			if(NextTemp!=NULL)
			{
				NextTemp=NextTemp->NextNode;
			}

			if(NextTemp->A_Node==A)
			{
				Temp->NextNode=NextTemp->NextNode;
				if(A==List->OpenedN->A_Node)
				{
					List->OpenedN=Temp->NextNode;
				}
				
				free(NextTemp);

				break;
			}
			Temp=Temp->NextNode;
		}
	}
	else
	{
		List->Opened0=Temp->NextNode;
		free(Temp);
	}
}
*/


void DelNodeClosedList(AList *List,Node *A)
{
	ANode *Temp=List->Closed0;
	ANode *NextTemp=List->Closed0;

	if(NextTemp->NextNode!=NULL) //Si la liste ne contient pas qu'un seul élement
	{
		NextTemp=Temp->NextNode;
	}

	if(A!=List->Closed0->A_Node)
	{
		while(NextTemp->A_Node!=A)
		{
			NextTemp=NextTemp->NextNode;
			Temp=Temp->NextNode;
		}
		
		
		if(A==List->ClosedN->A_Node) // ~ NextTemp==List->ClosedN
		{
			List->ClosedN=Temp;
		}

		Temp->NextNode=NextTemp->NextNode;
		NextTemp->NextNode=NULL;
		free(NextTemp);
		NextTemp=NULL;

	}
	else
	{
		List->Closed0=Temp->NextNode;
		free(Temp);
		Temp=NULL;
	}

}







/*

void DelNodeClosedList(AList *List,Node *A)
{
	ANode *Temp=List->Closed0;
	ANode *NextTemp=Temp;
	
	if(A!=List->Closed0->A_Node)
	{
		while(Temp!=NULL)
		{
			if(NextTemp!=NULL)
			{
				NextTemp=NextTemp->NextNode;
			}

			if(NextTemp->A_Node==A)
			{
				Temp->NextNode=NextTemp->NextNode;
				free(NextTemp);
				break;
			}
			Temp=Temp->NextNode;
		}
	}
	else
	{
		List->Closed0=Temp->NextNode;
		free(Temp);
	}
}

*/

void DelAList(AList *List)
{
	ANode *Temp=NULL;
	ANode *NextTemp=NULL;
	//Suppression Opened List
	Temp=List->Opened0;
	NextTemp=Temp;
	while(NextTemp!=NULL)
	{
		NextTemp=NextTemp->NextNode;
		free(Temp);
		Temp=NextTemp;
	}

	//Suppresion Closed List
	
	Temp=List->Closed0;
	NextTemp=Temp;
	while(NextTemp!=NULL)
	{
		NextTemp=NextTemp->NextNode;
		free(Temp);
		Temp=NextTemp;
	}

	//Suppression Path
	
	Temp=List->Path0;
	NextTemp=Temp;
	while(NextTemp!=NULL)
	{
		NextTemp=NextTemp->NextNode;
		free(Temp);
		Temp=NextTemp;
	}

	free(List);
	List=NULL;

}


char IsInOpenedList(AList *List,Node *A)
{
	ANode *Temp=List->Opened0;

	while(Temp!=NULL&&Temp->A_Node!=A)
	{
		Temp=Temp->NextNode;
	}
	if(Temp!=NULL)
	{	
		if(Temp->A_Node==A && A!=NULL)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}



char IsInClosedList(AList *List,Node *A)
{
	ANode *Temp=List->Closed0;

	while(Temp!=NULL&&Temp->A_Node!=A)
	{
		Temp=Temp->NextNode;
	}
	if(Temp!=NULL)
	{	
		if(Temp->A_Node==A && A!=NULL)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
}


char IsInPath(AList *List,Node *A)
{
	ANode *Temp=List->Path0;

	while(Temp!=NULL&&Temp->A_Node!=A)
	{
		Temp=Temp->NextNode;
	}
	
	if(Temp->A_Node==A && A!=NULL)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}



double EulerDistance(Node *A,Node *B)
{
	return sqrt(pow(A->Gridx-B->Gridx,2.)+pow(A->Gridy-B->Gridy,2.));	
}


double ManhattanDistance(Node *A,Node *B)
{
	return (double)labs(A->Gridx-B->Gridx)+labs(A->Gridy-B->Gridy);
}

double NullDistance(Node *A,Node *B)
{
	return 0;
}


void UpDateOpenedList(AList *List,ANode *Ref)
{
	//Un noeud valide est un noeud qui n'appartient pas à la closed list et qui
	//est du type Path,Fog ou EndZone
	
	//Si le noeud adjacent existe ET qu'il n'est pas dans la closedList ET qu'il est accessible ET n'appartient pas déjà l'openedlist
	
	if(Ref->A_Node->NextNodex!=NULL)
	{
		if(!IsInClosedList(List,Ref->A_Node->NextNodex)&&(Ref->A_Node->NextNodex->Kind==Path || Ref->A_Node->NextNodex->Kind==Fog || Ref->A_Node->NextNodex->Kind==EndZone)&&!IsInOpenedList(List,Ref->A_Node->NextNodex))
		{
			AddNodeOpenedList(List,Ref->A_Node->NextNodex);
			List->OpenedN->Father=Ref;
		}
	}

	if(Ref->A_Node->NextNodey!=NULL)
	{
		if(!IsInClosedList(List,Ref->A_Node->NextNodey)&&(Ref->A_Node->NextNodey->Kind==Path || Ref->A_Node->NextNodey->Kind==Fog || Ref->A_Node->NextNodey->Kind==EndZone)&&!IsInOpenedList(List,Ref->A_Node->NextNodey))
		{
			AddNodeOpenedList(List,Ref->A_Node->NextNodey);		
			List->OpenedN->Father=Ref;
		}
	}

	if(Ref->A_Node->PrevNodex!=NULL)
	{
		if(!IsInClosedList(List,Ref->A_Node->PrevNodex)&&(Ref->A_Node->PrevNodex->Kind==Path || Ref->A_Node->PrevNodex->Kind==Fog || Ref->A_Node->PrevNodex->Kind==EndZone)&&!IsInOpenedList(List,Ref->A_Node->PrevNodex))
		{
			AddNodeOpenedList(List,Ref->A_Node->PrevNodex);		
			List->OpenedN->Father=Ref;
		}
	}

	if(Ref->A_Node->PrevNodey!=NULL)
	{
		if(!IsInClosedList(List,Ref->A_Node->PrevNodey)&&(Ref->A_Node->PrevNodey->Kind==Path || Ref->A_Node->PrevNodey->Kind==Fog || Ref->A_Node->PrevNodey->Kind==EndZone)&&!IsInOpenedList(List,Ref->A_Node->PrevNodey))
		{
			AddNodeOpenedList(List,Ref->A_Node->PrevNodey);		
			List->OpenedN->Father=Ref;
		}
	}
	
}

ANode *SelectNode(AList *List) // On sélectionne le meilleur noeud (f minimal)
{
	ANode *Temp=List->Opened0;
	ANode *Selected=List->Opened0;

	while(Temp!=NULL)
	{	
		if(Temp->f<=Selected->f)
		{
			Selected=Temp;		
		}

		Temp=Temp->NextNode;
	}
	AddNodeClosedList(List,Selected->A_Node);
	
	if(List->ClosedN!=NULL)
	{
		List->ClosedN->Father=Selected->Father;
	}
	DelNodeOpenedList(List,Selected->A_Node);
	
	return List->ClosedN;
}


void ApplyF(AList *List,Node *Ref,Node *End) //Appliquer f à toute l'open list
{

	ANode *Temp=List->Opened0;
	
	while(Temp!=NULL)
	{
		Temp->f=(*(List->g))(Ref,Temp->A_Node)+(*(List->h))(Temp->A_Node,End);
		Temp=Temp->NextNode;
	}
}

void PathFinding(FlagMap *Map,AList *List,Node *Start,Node *End,double (*g)(Node *,Node *),double (*h)(Node*,Node*)) //Valable avec maillage 2 axes orthogonaux
{
	//Initialisation
	
	InitAList(List,Start,g,h);
	//InitAList ajoute Start à l'opened list	
	
	ANode *Temp=List->Opened0;
	//Début
	while(List->Opened0!=NULL&&Temp->A_Node!=End)
	{
		//Recherche des noeuds voisin correct
		
		Temp=SelectNode(List);
		UpDateOpenedList(List,Temp);
		ApplyF(List,Start,End);	
	}

	while(Temp!=NULL)
	{
		AddNodePath(List,Temp->A_Node);
		printf("PathValue: %p (%ld,%ld) (%2f,%2f)\n",Temp->A_Node,Temp->A_Node->Gridx,Temp->A_Node->Gridy,Temp->A_Node->x,Temp->A_Node->y);	
		Temp=Temp->Father;
	}

	DebugPath(List);


}

// DEBUG


void DebugOpenedList(AList *List)
{
	ANode *Temp=List->Opened0;
	printf("Opened List:\n\n");
	while(Temp!=NULL)
	{
		printf("Value: %p Current: %p  Next:  %p  F=%f\n",Temp->A_Node,Temp,Temp->NextNode,Temp->f);
		Temp=Temp->NextNode;
	}
}

void DebugClosedList(AList *List)
{
	ANode *Temp=List->Closed0;
	printf("Closed List:\n\n");
	while(Temp!=NULL)
	{
		printf("Value: %p Current: %p  Next:  %p  F=%f\n",Temp->A_Node,Temp,Temp->NextNode,Temp->f);
		Temp=Temp->NextNode;
	}
}


void DebugPath(AList *List)
{
	ANode *Temp=List->Path0;
	printf("Path:\n\n");
	while(Temp!=NULL)
	{
		printf("Value: %p Current: %p  Next:  %p  (%ld,%ld)\n",Temp->A_Node,Temp,Temp->NextNode,Temp->A_Node->Gridx,Temp->A_Node->Gridy);
		Temp=Temp->NextNode;
	}
}



