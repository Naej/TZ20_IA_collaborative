#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include "Color.h"
#include "Screen.h"
#include "Draw.h"
#include "AStar.h"
#include "Mesh.h"
#include "Condition.h"
#include "MathSpec.h"

#include "Presentation.h" //Pour la revue de projet uniquement

#define WIDTH 800
#define HEIGHT 600

int InitScreen(const char *WinName,int WxPos,int WyPos,int WindowWidth,int WindowHeight,Uint32 WindowFlag,SDL_Window **scr,SDL_Renderer **rndr)
{
	SDL_Rect Rec;
	Rec.x=0;
	Rec.y=0;
	Rec.w=WindowWidth;
	Rec.h=WindowHeight;
	SDL_Init(SDL_INIT_VIDEO);
	*scr = SDL_CreateWindow(WinName,WxPos,WyPos,WindowWidth,WindowHeight,WindowFlag);
	*rndr= SDL_CreateRenderer(*scr,-1,SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(*rndr,0,0,0,255);
	SDL_RenderFillRect(*rndr,&Rec);
	SDL_RenderPresent(*rndr);


}	
// FONCTION DE DEBUG


int main (int argc, char *argv[])
{

	

	//SDL_Init (SDL_INIT_VIDEO); // Initialise la SDL, retourne une valeur négative en cas d'érreur

	SDL_Window *window;
	SDL_Renderer *renderer;

	Screen MainScreen;
	MainScreen.window=&window;
	MainScreen.renderer=&renderer;


	InitScreen("TestDijkstra",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,WIDTH,HEIGHT,SDL_WINDOW_RESIZABLE,&window,&renderer);			       




	//1er affichage
	
	FlagMap *Map=(FlagMap*)malloc(sizeof(FlagMap));
	MapU(Map);
	DebugMapZ(Map);
	//A* 
	AList *ListH=(AList*)malloc(sizeof(AList));
	AList *ListD=(AList*)malloc(sizeof(AList));

	PathFinding(Map,ListH,GetNode(Map,0,10),GetNode(Map,50,90),EulerDistance,EulerDistance);	
	PathFinding(Map,ListD,GetNode(Map,0,10),GetNode(Map,50,90),EulerDistance,NullDistance);

	
	// Affichage et attente d'evenement de sortie
	DispAndWait(&MainScreen,Map,ListH,ListD);
	//Désalocation
	DeleteFlagMap(Map); 	
	DelAList(ListH);
	DelAList(ListD);
	
	
	/*
	//2ème affichage
	
	FlagMap * Map2=(FlagMap*)malloc(sizeof(FlagMap));
	MapU(Map2);
	DebugMapZ(Map2);
	//A* 
	AList *ListH2=(AList*)malloc(sizeof(AList));
	AList *ListD2=(AList*)malloc(sizeof(AList));

	//PathFinding(Map,ListH,GetNode(Map,0,90),GetNode(Map,224,-12),EulerDistance,EulerDistance);	
	//PathFinding(Map,ListD,GetNode(Map,0,90),GetNode(Map,224,-12),EulerDistance,NullDistance);

	PathFinding(Map2,ListH2,GetNode(Map,10,45),GetNode(Map,70,50),EulerDistance,EulerDistance);
	PathFinding(Map2,ListD2,GetNode(Map,10,45),GetNode(Map,70,50),EulerDistance,NullDistance);
	
	// Affichage et attente d'evenement de sortie
	DispAndWait(&MainScreen,Map2,ListH2,ListD2);
	//Désalocation
	DeleteFlagMap(Map2); 	
	DelAList(ListH2);
	DelAList(ListD2);

*/	
	


	//Destruction SDL	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	SDL_Quit();

	
	return 0;
}
