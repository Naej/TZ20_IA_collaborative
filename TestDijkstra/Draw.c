/*********************************************************************************
*     File Name           :     Draw.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-09 14:40]
*     Last Modified       :     [2017-01-14 22:24]
*     Description         :     Fonctions de dessins 
**********************************************************************************/
#include <math.h>
#include "Draw.h"
#include "Mesh.h"
#include "MathSpec.h"
#include "AStar.h"

void Line(Screen *scr,int xa,int ya,int xb,int yb,Color Clr)
{
	SDL_SetRenderDrawColor(*(scr->renderer),Clr.Red,Clr.Green,Clr.Blue,Clr.Alpha);
		
	SDL_RenderDrawLine(*(scr->renderer),xa,ya,xb,yb);
	SDL_RenderPresent(*(scr->renderer));
}

void FilledRect(Screen *scr,int xa,int ya,int w,int h,Color Clr)
{
	/*
	int ww,wh;
	SDL_GetWindowSize(&ww,&wh);
	*/
	SDL_Rect Rect;
	Rect.x=xa;
	Rect.y=ya;
	Rect.w=w;
	Rect.h=h;

	SDL_SetRenderDrawColor(*(scr->renderer),Clr.Red,Clr.Green,Clr.Blue,Clr.Alpha);
	SDL_RenderFillRect(*(scr->renderer),&Rect);
	SDL_RenderPresent(*(scr->renderer));
}

void FilledCircle(Screen *scr,int xa,int ya,int radius,Color Clr)
{
	
	float i=0,j=radius;
	//float p=1/(4*M_PI*radius);
	float p=1/(4*M_PI);	
	float Maxi=2*M_PI*radius;
	SDL_SetRenderDrawColor(*(scr->renderer),Clr.Red,Clr.Green,Clr.Blue,Clr.Alpha);
	
	for(j=radius;j>=0;j--)
	{
		for(i=0;i<Maxi;i+=p/8)
		{
			SDL_RenderDrawPoint(*(scr->renderer),xa+j*cos(i),ya+j*sin(i));
		}
	}

	SDL_RenderPresent(*(scr->renderer));

}


void DrawGridPoint(Screen* scr,FlagMap* Map,int Gridx,int Gridy)
{

	int ww,wh;
	
	SDL_GetWindowSize(*(scr->window),&ww,&wh);


	//SDL_RenderDrawPoint(*(scr->renderer),(int)Range((double)ww,0.,(double)Map->GridxMax*Map->Gap,(double)Map->GridxMin*Map->Gap,Gridx*Map->Gap),(int)Range(0.,(double)wh,(double)Map->GridyMax*Map->Gap,(double)Map->GridyMin*Map->Gap,(double)Gridy*Map->Gap));

	SDL_RenderDrawPoint(*(scr->renderer),(int)Range((double)ww,0.,(double)Map->GridxMax,(double)Map->GridxMin,Gridx),(int)Range(0.,(double)wh,(double)Map->GridyMax,(double)Map->GridyMin,(double)Gridy));
}


void DrawGridLine(Screen *scr,FlagMap *Map,long int Gridx1,long int Gridy1,long int Gridx2,long int Gridy2)
{
	int ww,wh;

	SDL_GetWindowSize(*(scr->window),&ww,&wh);
	
	//Expression des paramètres x,y du segment
	//de vecteur directeur normalisé (a,b)

	if(Gridx1 != Gridx2 && Gridy1 != Gridy2)
	{

		double i=0.f;

		double x=0.f;
		double y=0.f;
		double Length=sqrt((Gridy1-Gridy2)*(Gridy1-Gridy2)+(Gridx1-Gridx2)*(Gridx1-Gridx2));

		double a=(Gridx2-Gridx1)/(Length);
		double b=(Gridy2-Gridy1)/(Length);



		for(i=0;i<=Length;i++)
		{
			x=i*a+Gridx1;
			y=i*b+Gridy1;

			SDL_RenderDrawPoint(*(scr->renderer),(int)Range((double)ww,0.,(double)Map->GridxMax,(double)Map->GridxMin,((int)x)),(int)Range(0.,(double)wh,(double)Map->GridyMax,(double)Map->GridyMin,(double)(((int)y))));

		}

	}
	

}

void DrawMap(Screen *scr,FlagMap *Map) // O(2n) rectangulaire => O(n) toute forme
{
	int ww,wh;
	
	SDL_GetWindowSize(*(scr->window),&ww,&wh);
	//FilledRect(scr,0,0,ww,wh,GetColor(80,80,80,255));
	FilledRect(scr,0,0,ww,wh,DARK);
	Node *Cursor=Map->Node0;

//	DebugMapZ(Map);
	while(Cursor->NextNodez!=NULL)
	{
		if(Cursor->Kind==Path)
		{
    			SDL_SetRenderDrawColor(*(scr->renderer),255,255,255,255);
		}
		else if(Cursor->Kind==Obstacle)
		{
    			SDL_SetRenderDrawColor(*(scr->renderer),255,0,0,255);
		}
		DrawGridPoint(scr,Map,Cursor->Gridx,Cursor->Gridy);
		//SDL_RenderDrawPoint(*(scr->renderer),(int)Range((double)ww,0.,(double)Map->GridxMax*Map->Gap,(double)Map->GridxMin*Map->Gap,Cursor->x),(int)Range(0.,(double)wh,(double)Map->GridyMax*Map->Gap,(double)Map->GridyMin*Map->Gap,(double)Cursor->y));
		Cursor=Cursor->NextNodez;
	}
	
}

void DrawPath(Screen *scr,FlagMap *Map,AList *List,Color MyColor)
{
	
	int ww,wh;
	
	SDL_GetWindowSize(*(scr->window),&ww,&wh);
	SDL_SetRenderDrawColor(*(scr->renderer),MyColor.Red,MyColor.Green,MyColor.Blue,MyColor.Alpha);

	ANode *Temp =List->Path0;
	ANode *Temp2=List->Path0->NextNode;
	while(Temp2!=NULL)
	{
		SDL_RenderDrawLine(*(scr->renderer),
		(int)Range((double)ww,0.,(double)Map->GridxMax*Map->Gap,(double)Map->GridxMin*Map->Gap,Temp->A_Node->x),
		(int)Range(0.,(double)wh,(double)Map->GridyMax*Map->Gap,(double)Map->GridyMin*Map->Gap,(double)Temp->A_Node->y),
		(int)Range((double)ww,0.,(double)Map->GridxMax*Map->Gap,(double)Map->GridxMin*Map->Gap,Temp2->A_Node->x),
		(int)Range(0.,(double)wh,(double)Map->GridyMax*Map->Gap,(double)Map->GridyMin*Map->Gap,(double)Temp2->A_Node->y));
		Temp=Temp->NextNode;
		Temp2=Temp2->NextNode;
	}
}

