/*********************************************************************************
*     File Name           :     Target.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-05 23:58]
*     Last Modified       :     [2016-10-06 00:08]
*     Description         :     Définition des fonctions de l'objectif 
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "Target.h"


void Init_Target(Target *Obj)
{
	Obj->Radius=0;
	Obj->AbsoluteAngle=0;
	Obj->RelativeAngle=0;
	Obj->X=0;
	Obj->Y=0;
}


void Set_PolarTarget(Target *Obj,float Radius,float AbsoluteAngle)
{
	Obj->X=(long int)Radius*cos(AbsoluteAngle);
	Obj->Y=(long int)Radius*sin(AbsoluteAngle);
}

void Set_CartTarget(Target *Obj,long int x,long int y)
{
	Obj->Radius=sqrt(x*x+y*y);
	if(x>0)
	{
		Obj->AbsoluteAngle=atan(y/x);
	}
	else if(x<0)
	{
		Obj->AbsoluteAngle=M_PI-atan(y/x);
	}
	else if(y>0)
	{
		Obj->AbsoluteAngle=M_PI/2;
	}
	else
	{
		Obj->AbsoluteAngle=-M_PI/2;
	}
}



