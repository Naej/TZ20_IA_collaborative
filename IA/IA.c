/*********************************************************************************
*     File Name           :     IA.c
*     Created By          :     robin
*     Creation Date       :     [2016-10-05 23:42]
*     Last Modified       :     [2016-10-06 00:07]
*     Description         :     Fonction de l'IA 
**********************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "IA.h"


void Init_IA(IA *Robot,float Radius,float AbsoluteAngle,unsigned int ID)
{

	Robot->X=(long int)Radius*cos(AbsoluteAngle);
	Robot->Y=(long int)Radius*sin(AbsoluteAngle);
	Robot->ID=ID;
}


void Explorer(IA *Robot) // Exploration de la carte via algorithmes D*
{



}

void Talk(IA *Robot,unsigned int ID) //Parler à un robot d'id 'ID'
{

}

void Broadcast(IA *Robot) // Parler à tous
{

}

