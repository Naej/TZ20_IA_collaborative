/*********************************************************************************
*     File Name           :     IA.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-05 20:42]
*     Last Modified       :     [2016-10-05 23:56]
*     Description         :     Définition des fonctions concernant la structure IA 
**********************************************************************************/

#ifndef  IA_INCLUDED_H
#define IA_INCLUDED_H
#include <math.h>
#include "Target.h"


struct IA
{
	float Radius,AbsoluteAngle,RelativeAngle; //Système de coordonnés polaire dont le repère est lié à une balise 
	long int X, Y; // Précotion du long int dans l'hypothèse d'un maillage fin
			//(0,0) correspond à la balise
	unsigned int ID; // Un identifiant pour chaque machine

};

typedef struct IA IA;


#endif

