/*********************************************************************************
*     File Name           :     Target.h
*     Created By          :     robin
*     Creation Date       :     [2016-10-05 23:23]
*     Last Modified       :     [2016-10-06 00:02]
*     Description         :     Définition de la structure target
*     				Cette structure représente les points d'objectifs que 
*     				doivent atteindre les robots 
**********************************************************************************/
#ifndef TARGET_INCLUDED_H
#define TARGET_INCLUDED_H
#include <math.h>


struct Target
{
	float Radius,AbsoluteAngle,RelativeAngle;
	long int X,Y;
};

typedef struct Target Target;




#endif

