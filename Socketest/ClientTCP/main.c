#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "client.h"

static void init(void)
{
#ifdef WIN32
   WSADATA wsa;
   int err = WSAStartup(MAKEWORD(2, 2), &wsa);
   if(err < 0)
   {
      puts("WSAStartup failed !");
      exit(EXIT_FAILURE);
   }
#endif
}

static void end(void)
{
#ifdef WIN32
   WSACleanup();
#endif
}

static void app(int NumberOfHosts , char HostNames[][MAX_NAME])
{
    SOCKET servers [MAX_SERV]; 
    SOCKET local;
    char MyName[MAX_NAME];
    gethostname(MyName,MAX_NAME);

	int i = 0;
	for (i=0;i<NumberOfHosts;i++){
		servers[i] = init_connection(HostNames[i]);
	}
    local = init_connection(MyName);

   	char buffer[BUF_SIZE];

   	fd_set rdfs;

   /* send our name */
    for (i=0;i<NumberOfHosts;i++){
	   write_server(servers[i],MyName);
	}
    write_server(local,MyName);


    while(1)
    {

    	FD_ZERO(&rdfs);

	    FD_SET(STDIN_FILENO, &rdfs);

	   	for (i=0;i<NumberOfHosts;i++){
	   		FD_SET(servers[i], &rdfs);
		}
        FD_SET(local,&rdfs);
		

      int max = local;

		for (i=0;i<NumberOfHosts;i++){
	    	if (max < servers[i]){
                max = servers[i];
            }
	    }
      if(select(max + 1, &rdfs, NULL, NULL,NULL) == -1) //fonction bloquante
        {
            perror("select()");
            exit(errno);
        }


	    if(FD_ISSET(STDIN_FILENO, &rdfs))
	    {

	       	fgets(buffer, BUF_SIZE - 1, stdin);
	       	{
	          	char *p = NULL;
	          	p = strstr(buffer, "\n");
	          	if(p != NULL)
	           	{
	           		*p = 0;
	           	}
	           	else
	           	{
	           		buffer[BUF_SIZE - 1] = 0;
	           	}
	       	}
	    	write_server(local, buffer);/*on écrit que en local les autres lisent dessus*/
		}	
	    for (i=0;i<NumberOfHosts;i++){

          	if(FD_ISSET(servers[i], &rdfs))
	      	{
	         	int n = read_server(servers[i], buffer); 
	         	if(n == 0)
	         	{
	            	printf("Server disconnected !\n");
	            	//break;
	            }
	         	puts(buffer);
	      	}
  		
      	
      	}
	
   	}
   	for (i=0;i<NumberOfHosts;i++){
   		end_connection(servers[i]);
   	}
    end_connection(local);
//    clearIpServ(NumberOfHosts,HostNames);

}

void clearIpServ(int NumberOfHosts , char HostNames[][MAX_NAME]){
    printf("---clearing serv files---");
    if (NumberOfHosts == 0){
        system("ssh -i id_serv root@192.168.2.1 rm /etc/www/ip.txt \n ssh -i id_serv root@192.168.2.1 touch /etc/www/ip.txt");
    }else{
        char command[MAX_NAME 
            + strlen("ssh -i id_serv root@192.168.2.1 'echo  >>/etc/www/ip.txt                                                                        '")];
        command[0]=0;
        strcat(command,"ssh -i id_serv root@192.168.2.1 'echo ");
        strcat(command,HostNames[0]);
        strcat(command, " >/etc/www/ip.txt'");
        system(command);
        int i =0;
        for (i=1;i<NumberOfHosts;i++){
            command[0]=0;
            strcat(command,"ssh -i id_serv root@192.168.2.1 'echo ");
            strcat(command,HostNames[i]);
            strcat(command, " >>/etc/www/ip.txt'");
            system(command);
                
        }
     //   write_server(local, "please refresh");
    }
}

static int init_connection(const char *address)
{
   SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
   SOCKADDR_IN sin = { 0 };
   struct hostent *hostinfo;
	printf("Connecting to : %s\n",address);
   if(sock == INVALID_SOCKET)
   {
      perror("socket()");
      exit(errno);
   }

   hostinfo = gethostbyname(address);
   if (hostinfo == NULL)
   {
      fprintf (stderr, "Unknown host %s.\n", address);
      exit(EXIT_FAILURE);
   }

   sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
   sin.sin_port = htons(PORT);
   sin.sin_family = AF_INET;

   	if(connect(sock,(SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
   	{
      	perror("connect()");
      	exit(errno);
   	}

   return sock;

}

static void end_connection(int sock)
{
   closesocket(sock);
}

static int read_server(SOCKET sock, char *buffer)
{
   int n = 0;

   if((n = recv(sock, buffer, BUF_SIZE - 1, 0)) < 0)
   {
      perror("recv()");
      exit(errno);
   }

   buffer[n] = 0;

   return n;
}

static void write_server(SOCKET sock, const char *buffer)
{

   if(send(sock, buffer, strlen(buffer), 0) < 0)
   {
      perror("send()");
      exit(errno);
   }
//   printf("send\n");
}

int main(int argc, char **argv)
{
   	char Name[MAX_NAME];
   	char HostNames[MAX_SERV][MAX_NAME];
   	gethostname(Name,MAX_NAME);
   	printf("Votre hostname est : %s\n",Name);
    
    /*
    char command[strlen(Name) 
        + strlen("ssh -i id_serv root@192.168.2.1 'echo  >>/etc/www/ip.txt                                                                        '")];
    command[0]=0;
    strcat(command,"ssh -i id_serv root@192.168.2.1 'echo ");
    strcat(command,Name);
    strcat(command, " >>/etc/www/ip.txt'\n rm ip.txt \n wget 192.168.2.1:8080/ip.txt");
    system(command);
    */

    system("rm ip.txt \n wget 192.168.2.1:8080/ip.txt");

   	FILE * config;
   	config = fopen("./ip.txt","r");

   	if (config == NULL)
   	{
      	printf("Pas de fichier de configuration !\n");
      	return EXIT_FAILURE;
      	exit(EXIT_FAILURE);
   	}
   	int i = 0;
   	while (fgets(HostNames[i],MAX_NAME,config) != NULL)
	{
		HostNames[i][strlen(HostNames[i])-1]='\0';
    	if (strcmp(Name,HostNames[i])){
    		printf("%s\n",HostNames[i]);
        	i++;
		}

	}
    fclose(config);

   	init();

   	app(i, HostNames);

   	end();

   	return EXIT_SUCCESS;
}
